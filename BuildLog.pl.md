# BuildLog
Read this in different language: [English](README.md),[Polish](README.pl.md)

###### 17.10.2016
Zakupiłem żyroskop i rozważam użycie Raspberry Pi + BrickPi.

###### 25.10.2016 1:28
NXT nie działa. Nowe baterie nie pomagają. Słychać tylko bardzo ciche piszczenie w środku.
Połączyłem RPI, brickpi i serwo. Cały dzień walki z javą na nic. Serwo wcale nie działa.
Z pythona zaskoczyło od razu. Może poddać się z javą i napisać to w pythonie?

###### 25.10.2016 17:36
Walka z RPI trwa. Mam dwa obrazy

1. Jakas odmiana raspbiana dostarczona przez DexterInd, twórców BrickPi - działa python, eclipse jest za wolny by używać, ale może jary zadziałają. Serwa dzialają
2. Ubuntu Mate - wgrana java, eclipse, python - Gdy sprawdzam czy działa z pytona dostaję:
`IOError: [Errno 25] Inappropriate ioctl for device`

###### 27.10.2016 20:10
http://forum.dexterindustries.com/t/rpi3-ubuntu-serial-connection-error/1982/4

###### 28.10.2016 10:22
Jak widac w powyższym linku, twórcy nie wspierają ubuntu. Dlatego wykorzystam ich raspbpiana.

###### 28.10.2016 23:33
NXT jednak działa, tylko bez ekranu, wgrałem lejosa ale nic wiecej nie zrobie chyba ze po omacku. Jak nic nie ruszy z BrickPi to spróbuje to jakoś przelutować, może pomoże...
Z BrickPi idzie ciężko. Nie bardzo wiem jakie biblioteki mam użyć żeby chociażby zakręcić motorkiem. Na raspbianie próbowalem odpalić program z repo brickpijava ale nie zadziałał. Próbowałem uruchomić ev3dev na rpi ale to wcale nie startuje. Coraz bardziej skłaniam sie do raspbpian+python, tylko tu odało mi się uruchmić cokolwiek.

###### 06.11.2016 14:12
BrickPi potrzebuje update aby obsługiwac żyroskop. Potrzebuję do tego kogoś z arduino uno.
Musze zrezygnowac z Javy. Nie dam rady odpalić tego na rpi. Nie wiem czemu ale te biblioteki jakos nie potrafią wykryc i obsłużyć sprzętu. Python działa bez problemu. Tylko to żyro naprawić.

###### 14.11.2016 22:42
Próbowalem dzis updatować oprogramowanie brickpi. Nie wyszło za dobrze.
`avrdude: stk500_getsync(): not in sync: resp=0x00`
W piątek powtórka. Wiem juz co zrobiłem źle (jednak bez rpi i brickpi musi byc zasilane z baterii) ale nie wiem czy to jest przyczyną niepowodzenia.

###### 16.11.2016 21:05
Czemu ja mam tyle problemów?
Pożyczyłem programator usbasp;
Podpinam do kompa - windows nie widzi urządzenia;
Instaluje sterowniki, uruchamiam skrypt update - avrdude nie widzi urządzenia;
Grzebie w necie, zmieniam w skrypcie urządzenie na usbasp i uruchamiam - dalej nic;
Znowu grzebie w necie, znalazlem inny sterownik, ściągam i wgrywam - windows krzyczy ze niebezpieczny sterownik;
Reboot, wyłączam sprawdzanie sterów, instalacja sukces, odpalam - `cannot set sck period`....
Znowu grzebie w necie, teraz mówią zeby to zignorowac. Ale to co dostaję jako wynik działania avrdude nie pokrywa się z tym co dexter ind. pokazali.
obracanie silnikiem dalej działa, gyro dalej nie.
Link do forum gdzie szukam pomocy [na forum]( http://forum.dexterindustries.com/t/updating-firmware-to-the-latest-version/1763/12)

###### 22.11.2016 17:07
Dalej szukam pomocy na forum...

###### 28.11.2016 10:42
http://forum.dexterindustries.com/t/update-brickpi-fw-with-usbasp/2122/4
W piątek założyłem osobny temat na forum. Dzis odpisali. Okazuje sie ze skrypt z ktorego korzystalem z jakiegos powodu nie odpalał komend updatujących.
Zrobił coś tylko z 'fuses'. Cokolwiek to znaczy... Zrobilem to ręcznie i wyniki były obiecujące. Jednak test pythonem zwrócił error.
Efekt jest taki ze teraz nie działa nic. Zobacze co odpowiedzą na to twórcy.

###### 5.12.2016 11:45
Twórcy odpisali i powiedzieli gdzie zrobilem błąd. Nie dopisalem w jednej linijce EEPROM. Jednak, poprawiona linijka nie pomogła za bardzo.
Teraz program utyka na sprawdzaniu sensorów. Spróbuję jeszcze format i powrót to ostatniej działającej konfiguracji i czekam na ich odpowiedź.
http://forum.dexterindustries.com/t/update-brickpi-fw-with-usbasp/2122/5

###### 7.12.2016 18:26
Nic nie działa. Arduino nawet nie chce ruszyc z wgrywaniem update. Usbasp nie spisuje się i nie mogę nic uruchomic na raspi.
Powoli mam dość. Nic jeszcze nie mam zrobione poza robotem a czasu jest coraz mniej.

###### 10.12.2016 11:27
Wczoraj pożyczyłem jeszcze jeden usbasp od kolegi. Update przebiegał znacznie dłużej, tak z 5 min, ale i tak nic nie wyszło. Dalej zawiesza się na BrickPiSetupSensors().
Dziś spróbowałem jeszcze raz arduino. Poprzednio nie wgrałem na niego programu ISP wiec nic sie nie chcialo wgrywac. Tym razem zrobiłem kroki 1-4 z [tego tutoriala](https://www.arduino.cc/en/Tutorial/ArduinoISP), a następnie wgrałem fw na brickpi. Efekt ten sam co poprzednie kilka prób. Zawiesza sie na BrickPiSetupSensors()

###### 10.12.2016 19:08
Gdzies po drodze musiałem zrobic `apt-get upgrade` bo teraz mam raspbian PIXEL.
Okazuje sie ze PIXEL w pewien sposób wpływa i zakłóca działanie GPIO więc twórcy zasugerowali powrót do ich najnowszego obrazu.
Dobra wiadomosc - motory znowu się kręcą
Zła wiadomosc - `DOH! Please update your firmware to run EV3 sensors! You should be able to run NXT sensors, but not EV3 sensors.`
Czyli nie działa żyroskop

###### 13.12.2016 12:13
[DZIAŁA!!!!!!! WRESZCIE DZIAŁA](http://forum.dexterindustries.com/t/update-brickpi-fw-with-usbasp/2122/23?u=mike2060)

###### 13.12.2016 13:17
Problemy z zasilaniem. Dziwne rzeczy dzieją sie z akumulatorkami. Jeden zmienił polaryzacje,3 padły a 4 mają więcej niz 1.2V. Ładuję akumulatory...

###### 13.12.2016 15:38:30
Czy te chińskie aku mogą byc bateriami? każdy ma 1.5V ...
Motorki działają :)

###### 13.12.2016 18:36:44
motorRotateDegree kręci bez przerwy...

###### 13.12.2016 19:07:25
[Kill me ...](http://forum.dexterindustries.com/t/accuracy-of-rotating-a-motor/258/20)

###### 14.12.2016 8:01:02
Tworzyłem środowisko na laptopie, ustawienia pycharma, dodanie zdalnego serwera (rpi) i ustawienia zdalnego interpretera.
Interpreter się zaczął aktualizować. Po 10 minutach stiwerdziłem, że nie mam na to czasu i przerwałem. I robot przestał działać.
Nie obracały się silniki, nawet sie nie działało SetupSenors(). Odinstalowałem BrickPi i wgrałem ją na nowo. (biblioteka)

###### 14.12.2016 8:40:00
Na konsultacje wszedłem z nie działającym robotem. Do stycznia jeszcze powalczę. Może uda się zrobić innego, mniej skomplikowanego robota.

###### 14.12.2016 9:20:00
Przed kolejnymi zajęciami siadłem i zacząłem sprawdzać czemu nie działało. Wpisałem te same polecenia i działa... Nikt sie nie dowie już czemu nie działało wcześniej.

###### 17.12.2016 18:53:09
Nie jest dobrze...

Metoda updateValues() w tym samym czasie obraca serwem i odczytuje z sensorów (żyroskop). To niedopuszczalne dla samobalansującego robota.

Prosty projekt jak simplebot nie działa poprawnie. Poniżej komendy i co powinny robic, a na prawo od nich co sie faktycznie dzieje.
Kod dostępny tu: [link do githuba](https://github.com/DexterInd/BrickPi_Python/blob/master/Project_Examples/simplebot/simplebot_simple.py)

| Komenda | Co powinno się dziać | Co się dzieje |
|:-------:|:--------------------:|:-------------:|
|w        |oba silniki do przodu |prawy do przodu|
|a        |prawy silnik do przodu|lewy do przodu |
|d        |lewy silnik do przodu |nic            |
|s        |oba silniki do tyłu   |nic            |
|x        |wyjdź z programu      |oba do tyłu    |
|ctrl+c   |wyjdź z programu      |wychodzi ale silniki dalej sie kręcą|
|a jako pierwsze polecenie po uruchomieniu|prawy silnik do przodu |oba silniki do tyłu|
[Sterowanie robotem - simplebot_simple]

To, jaka była poprzednia komenda wpływa na zachowanie następnej. s po w zatrzyma silniki.

Rozpocząłem kolejny wątek na ich forum dostępny pod [tym adresem](http://forum.dexterindustries.com/t/serious-problems-with-brickpi/2211)
Podejrzewam ze troche im zajmie odpisanie. W tym czasie spróbuję wgrać inną bibliotekę ev3dev.

###### 19.12.2016 15:45:53
Wypróbowałem zaproponowany kod i wcale nie jest lepiej. Najpierw oba silniki kręca trochę do przodu, a potem tylko jeden kręci do tyłu bez końca

###### 19.12.2016 23:44:12
Odezwał się do mnie użytkownik forum i podesłał mi swój kod (graykevinb_solution.py). Jego rozwiązanie na motorRotateDegree również u mnie nie działa. Może problemem jest hardware silników? Może należy użyć tych nowszych?

###### 23.12.2016 23:32:20
EV3DEV. Wgrałem na karte i odpaliłem (nie bez problemów) - system działa.
Reszta? Nie. Zaraz po starcie rpi3 i po podłączeniu baterii silniki zaczynają się kręcic. Same z siebie.
Patrzyłem czy jakis python jest w procesach ale nic nie ma. Nie wiem co z tym zrobić.
Przykładowy programik do kręcenia serwem wyrzuca "device is not connected"
[Link do git issues](https://github.com/rhempel/ev3dev-lang-python/issues/279)

###### 27.12.2016 18:19:34
Kazano mi założyć nową sprawę na gicie. Jak na razie jest źle bo nic nie działa.
[Nowy link](https://github.com/ev3dev/ev3dev/issues/796)

###### 27.12.2016 23:19:42
Jest lepiej. Działają mi motorki. Także dam rade zrobić już simplebota. Nie działają jeszcze wszystkie sensory. Nie moze ich jakoś wykryć.
`Exception: Device is not connected`
[Nowy link](https://github.com/ev3dev/ev3dev/issues/797)

###### 28.12.2016 12:56:55
BrickPi na ev3dev nie umie samemu wykryć sensorów. Serwa ładowane są z automatu na starcie ale sensory trzeba ręcznie. Dlatego nie działało.
Udało mi się podłączyc jeden touch sensor. To już coś. Z drugi mam problem. I dalej nie wiem co z gyro.
[Link](https://github.com/ev3dev/ev3dev/issues/797)

###### 28.12.2016 21:37:42
No i jest dobry progress, z pythona da sie podłączyć dwa sensory dotyku. Gyro nie zbiera poprawnych danych. Ale mogę zrobic tego prostego robota jeżdżącego.
[Link](https://github.com/rhempel/ev3dev-lang-python/issues/281)

###### 29.12.2016 20:09:37
No to działa :)
Nie jest to robot balansujący ale robot sterowany przez przeglądarkę.
[Filmik](https://1drv.ms/v/s!AtR5ruC8xD0VgudhDcSQ3u0ntGLK8Q)

###### 29.12.2016 22:46:50
Robię porządki w plikach. Postanowiłem zrobić wersje anglojęzyczną by więcej ludzi mogło ominąć moje błędy. Eh...  teraz każdy kolejny wpis musze robić dwa razy :(

###### 30.12.2016 11:35:04
DexterInd zrobili parę dni temu aktualizacje biblioteki więc postanowiłem ją wypróbować. Niestety motorRotateDegree wciąż nie działa jak należy (albo znowu coś zrobiłem nie tak).
Motor power wpływa na kąt, który z resztą sam w sobie nie odpowiada temu o ile faktcznie sie obraca.
pwr=100, deg=90 to obrót o prawie 240deg.
pwr=200,deg=90 to obrót ponad 450deg.
Programy ev3dev nie mogą działać na raspbianie od DexterInd.

###### 30.12.2016 17:10:08
Wreszcie udało mi sie dodac 'emergencyBreak' - zatrzymuje koła gdy robot uderzy w przeszkode, i 'closeBtn' - ręcznie zatrzymuje robota i **zamyka** program.