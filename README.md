Read this in different language: [English](README.md),[Polish](README.pl.md)
***
# RaspberryPi 3 b + BrickPi + ev3dev + lego mindstorms
This project was made during my 3rd year computer science studies. I had many LEGO's laying around and decided to use it at last.
***
###### Project start: 17.10.2016
###### Project end: 15.01.2017
***
## [BUILDLOG](BuildLog.en.md)
***
## [Video link](https://1drv.ms/v/s!AtR5ruC8xD0VgudhDcSQ3u0ntGLK8Q)
***
### Latest running configuration:
* Hardware:
  - RaspberryPi  3 Model B v1.2
  - BrickPi (old model (maybe HW v1.7.3) but FW upgraded to 2.0)
  - NXT motors and sensors
  - EV3 gyro sensor

* Ev3dev img: *ev3dev-jessie-rpi2-generic-2016-12-21.img*
* Kernel: *4.4.32-17-ev3dev-rpi2*
* Host PC: *Windows10 pro*
* IDE: *PyCharm 2016.3
* I am connecting using: *wifi*
* My changes to config.txt (only enabled options):*
```
dtoverlay=brickpi
init_uart_clock=32000000
dtparam=brickpi_battery=okay
dtoverlay=pi3-disable-bt
dtparam=brickpi_led1_trigger=none,brickpi_led2_trigger=none
```
---
### What you will find in repository
* [**dex**](dex) - here are files I have used while on raspbian for robots (DexterInd image)
 - *BrickPi.py* - whole library
 - *graykevinb_solution* - solution for motorRotateDegree, not working
 - *gyrotest.py* - gyrotest
 - LEGO* - another solutions for motorRotateDegree, also not working
 - *mrd-solution* - another solution for motorRotateDegree, also not working
 - *simplebot_simple.py* - Example from DexterInd github
 - *test.py* - Simple motor test

* [**ev3dev**](ev3dev) - here are files I have used while on ev3dev raspbian
 - [**browserbot**](ev3dev/browserbot) - Robot controlled via browser. Details in its [readme](ev3dev/browserbot/README.md)
 - [**tests**](ev3dev/tests) - Various files I used to test what is working
     - *test.py* - Simplest motor test
     - *simplebot1.py* - tests adding sensors and gyro(commented out because it's not working) and motors
