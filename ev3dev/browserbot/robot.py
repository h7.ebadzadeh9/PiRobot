#!/usr/bin/env python3
###############################################################################################################
# Program Name: robot.html
# ===============================================
# This code is for controlling a robot by a web browser using web sockets
# ------------------------------------------------
# Original authors:
# Author     Comments
# Joshwa     Initial Authoring
# http://www.dexterindustries.com/
# https://github.com/DexterInd/BrickPi_Python/blob/master/Project_Examples/browserBot/
# ------------------------------------------------
# Adapted for ev3dev on brickpi my MikeDabrowski
# https://gitlab.com/MikeDabrowski/PiRobot/
# ------------------------------------------------
# These files have been made available online through a Creative Commons Attribution-ShareAlike 3.0  license.
# (http://creativecommons.org/licenses/by-sa/3.0/)#
###############################################################################################################
import ev3dev.brickpi as ev3
import threading
import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.template
import sys
from time import sleep

# Other variables
c = 0
speed = -500 # is negative because I placed motors upside down

def stoptornado():
	ml.stop()
	mr.stop()
	tornado.ioloop.IOLoop.instance().stop()

def emergencyBreak():
	print("emergencyBreak")
	ml.run_timed(time_sp=300, speed_sp=500) # When hits the wall it stops and no more commands can be received.
	mr.run_timed(time_sp=300, speed_sp=500) # This moves robot back a bit so user can send new commands.

# Start tornado
class MainHandler(tornado.web.RequestHandler):
	def get(self):
		loader = tornado.template.Loader(".")
		self.write(loader.load("index.html").generate())


# Code for handling the data sent from the webpage
class WSHandler(tornado.websocket.WebSocketHandler):
	def open(self):
		print('connection opened...')

	def check_origin(self, origin):
		return True

	def on_message(self, message):  # receives the data from the webpage and is stored in the variable message
		global c
		print('received:', message)  # prints the revived from the webpage
		if message == "u":  # checks for the received data and assigns different values to c which controls the movement of robot.
			c = "8";
		if message == "d":
			c = "2"
		if message == "l":
			c = "6"
		if message == "r":
			c = "4"
		if message == "b":
			c = "5"
		print(c)
		print(emergBtn.value())
		if emergBtn.value():
			print(emergBtn.value())
			ml.stop()
			mr.stop()
		elif c == '8':
			print("Running Forward")
			ml.run_forever(speed_sp=speed)
			mr.run_forever(speed_sp=speed)
		elif c == '2':
			print("Running Reverse")
			ml.run_forever(speed_sp=-speed)
			mr.run_forever(speed_sp=-speed)
		elif c == '4':
			print("Turning Right")
			ml.run_forever(speed_sp=speed)
			mr.stop()
		elif c == '6':
			print("Turning Left")
			ml.stop()
			mr.run_forever(speed_sp=speed)
		elif c == '5':
			print("Stopped")
			ml.stop()
			mr.stop()
		elif closeBtn.value():
			self.close()
			self.on_close()
			print("closed")

	def on_close(self):
		print('connection closed...')


application = tornado.web.Application([
	(r'/ws', WSHandler),
	(r'/', MainHandler),
	(r"/(.*)", tornado.web.StaticFileHandler, {"path": "./resources"}),
])

class myThread (threading.Thread):
	def __init__(self, threadID, name, counter):
		threading.Thread.__init__(self)
		self.threadID = threadID
		self.name = name
		self.counter = counter
	def run(self):
		print("Ready")
		#while not closeBtn.value():
		while running:
			if emergBtn.value():
				emergencyBreak()
			if closeBtn.value():
				stoptornado()
				print("Close BTN pressed")
				quit()
			sleep(.2)              # sleep for 200 ms
	def stop(self):
		self._stop.set()

if __name__ == "__main__":

	# Set motors
	ml = ev3.LargeMotor(ev3.OUTPUT_B) 		#Enable the Motor B
	mr = ev3.LargeMotor(ev3.OUTPUT_C) 		#Enable the Motor C

	# Set touch sensors for emergency stop
	p = ev3.LegoPort(ev3.INPUT_1)
	p.mode = "nxt-analog"
	p.set_device = "lego-nxt-touch"
	emergBtn = ev3.TouchSensor(ev3.INPUT_1)

	p2 = ev3.LegoPort(ev3.INPUT_2)
	p2.mode = "nxt-analog"
	p2.set_device = "lego-nxt-touch"
	closeBtn = ev3.TouchSensor(ev3.INPUT_2)

	# I tried to fix the issue that first run after boot has "Device is not connected", but I failed
	try:
		emergBtn.value()
	except Exception:
		print('Emergency button not loaded, reloading...')
		p = ev3.LegoPort(ev3.INPUT_1)
		p.mode = "nxt-analog"
		p.set_device = "lego-nxt-touch"

	try:
		closeBtn.value()
	except Exception:
		print('Close button not loaded, reloading...')
		p2 = ev3.LegoPort(ev3.INPUT_2)
		p2.mode = "nxt-analog"
		p2.set_device = "lego-nxt-touch"
		closeBtn = ev3.TouchSensor(ev3.INPUT_2)

	running = True
	thread1 = myThread(1, "Thread-1", 1)
	thread1.setDaemon(True)
	thread1.start()
	application.listen(9093)          	#starts the websockets connection
	tornado.ioloop.IOLoop.instance().start()
