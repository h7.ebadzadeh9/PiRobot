#!/usr/bin/env python
# Karan Nayan
# Initial Date: July 11, 2013
# Last Updated: July 11, 2013
# http://www.dexterindustries.com/
#
# These files have been made available online through a Creative Commons Attribution-ShareAlike 3.0  license.
# (http://creativecommons.org/licenses/by-sa/3.0/)
#
# This is an example of controlling the rotation of motors using encoders
from BrickPi import *   #import BrickPi.py file to use BrickPi operations

BrickPiSetup()  # setup the serial port for sudo su communication
BrickPiSetupSensors()       #Send the properties of sensors to BrickPi
BrickPiUpdateValues()

"""
Pass the arguments in a list. 
If a single motor has to be controlled then the arguments should be passed like 
elements of an array,e.g, motorRotateDegree([255],[360],[PORT_A]) where power=255 and
angle=360 for the motor connected at Port A
"""
#power=[255]
#deg=[360]
#port=[PORT_A]
#motorRotateDegree(power,deg,port)		#This read the encoder values every 100 ms (default). Not that accurate but not very processor intensive
#motorRotateDegree(power,deg,port,.05)	#This read the encoder values every 50 ms. A little more accurate 
#motorRotateDegree(power,deg,port,0)	#This read the encoder values without any delay. Most accurate but take a lot of processing power
#motorRotateDegree(power,deg,port,0,.04)	#This read the encoder values without any delay. The time to rotate the motors in the opposite direction when stopping is specified in the last argument. Useful when free running the motors

"""
If multiple motors have to be controlled then the parameters for running each motor must be passed
as the elements of an array,e.g, motorRotateDegree([255,100],[360,30],[PORT_A,PORT_B]) where 
power=255 and angle=30 are for motor at PORT_A and power=100 and angle=30 are for motor at PORT_B.
It can be used similarly for any number of motors.
"""

def motorRotateDeg(power,deg,port,sampling_time=.01,delay_when_stopping=.05):
    """Rotate the selected motors by specified degre
    Args:
      power    : an array of the power values at which to rotate the motors (0-255)
      deg    : an array of the angle's (in degrees) by which to rotate each of the motor
      port    : an array of the port's on which the motor is connected
      sampling_time  : (optional) the rate(in seconds) at which to read the data in the encoders
      delay_when_stopping:  (optional) the delay (in seconds) for which the motors are run in the opposite direction before stopping
        Returns:
        0 on success
        Usage:
        Pass the arguments in a list. if a single motor has to be controlled then the arguments             should be
      passed like elements of an array,e.g, motorRotateDegree([255],[360],[PORT_A]) or
      motorRotateDegree([255,255],[360,360],[PORT_A,PORT_B])
    """
    
    
    
    debug = True
    num_motor=len(power)    #Number of motors being used
    print num_motor
    init_val=[0]*num_motor
    curr_val=[0]*num_motor
    final_val=[0]*num_motor
    last_encod=[0]*num_motor
    
    delta=0
    gain=0.005
    idelta=0.0
    alpha=10
    smulti=0
    BrickPiUpdateValues()
    for i in range(num_motor):
        BrickPi.MotorEnable[port[i]] = 1        #Enable the Motors
        power[i]=abs(power[i])
        
        init_val[i]=BrickPi.Encoder[port[i]]        #Initial reading of the encoder  
        print "Motor " + `i` + " encoder: " + `init_val[i]`
        final_val[i]=init_val[i]+(deg[i]*2)        #Final value when the motor has to be stopped;One encoder value counts for 0.5 degrees
        print "Motor " + `i` + " encoder target: " + `final_val[i]`
        
        #For running clockwise and anticlockwise
        if deg[i]>0:
           BrickPi.MotorSpeed[port[i]] = power[i]
        elif deg[i]<0:
           BrickPi.MotorSpeed[port[i]] = -power[i]
        else:
           BrickPi.MotorSpeed[port[i]] = 0
        
        
    run_stat=[0]*num_motor
    
    while True:
        result = BrickPiUpdateValues()          #Ask BrickPi to update values for sensors/motors
        time.sleep(sampling_time)          #sleep for the sampling time given (default:10 ms)
        i = 0
        #if debug:
            #print "Result of Update Values: " + `result`
        if not result :
            for i in range(num_motor):        #Do for each of the motors
                #The FIRST thing we should do is check our encoders!
                curr_val[i]=BrickPi.Encoder[port[i]]
                if debug :
                    print "Motor " + `i` + " encoder: " + `curr_val[i]`
                        
                if run_stat[i]==1:
                    continue
                # Check if final value reached for each of the motors
                if(deg[i]>0 and final_val[i]<=curr_val[i]) or (deg[i]<0 and final_val[i]>=curr_val[i]) :
                    #This motor has reached its goal
                    run_stat[i]=1
                    
                    #Now let's hit the breaks by going in reverse for a VERY quick amount of time.
                    if deg[i]>0:
                        BrickPi.MotorSpeed[port[i]] = -power[i]
                    elif deg[i]<0:
                        BrickPi.MotorSpeed[port[i]] = power[i]
                    else:
                        BrickPi.MotorSpeed[port[i]] = 0            
                    BrickPiUpdateValues()
                    time.sleep(delay_when_stopping)
                    #Now let's turn the motor off all together
                    BrickPi.MotorEnable[port[i]] = 0
                    BrickPiUpdateValues()
        
        if(all(e==1 for e in run_stat)):        #If all the motors have already completed their rotation, then stop
          break
        
        #Let's use Proportional Integral Control on the Motors to keep them in Sync
        if i == 1 :
            if curr_val[0] <> 0 and curr_val[1] <>0 : 
                if last_encod[0]<>0 and last_encod[1] <>1 :
                    if abs(last_encod[0] - init_val[0]) < abs(last_encod[1] - init_val[1]) :
                        #Motor 1 is going faster
                        delta = abs(curr_val[1]-last_encod[1]) - abs(curr_val[0]-last_encod[0])
                        idelta = (abs(curr_val[1]-init_val[1]) - abs(curr_val[0]-init_val[0]))/alpha
                        if debug:
                            print "Motor 1 is faster by "  + `delta`
                            print "last_encod = " + `last_encod[0]` + " , " + `last_encod[1]`
                            print "idelta = " + `idelta`
                            print "Current Encode = " + `curr_val[0]` + " , " + `curr_val[1]`

                        if int(abs(BrickPi.MotorSpeed[port[0]])) == 255 :
                            #Motor 0 CANNOT be sped up
                            smulti=BrickPi.MotorSpeed[port[0]]*delta*gain+idelta*gain
                            #Speed Multiplier: the amount we want to slow down Motor 1
                            if int(abs(BrickPi.MotorSpeed[port[1]]-smulti)) <= 255 : 
                                #Target speed is inside the bounds of Motor speed
                                BrickPi.MotorSpeed[port[1]] = int (BrickPi.MotorSpeed[port[1]]-smulti)
                            elif int (BrickPi.MotorSpeed[port[1]]-smulti) < 0 :
                                #Target speed is outside the bounds of -255 to 255
                                BrickPi.MotorSpeed[port[1]] = -255
                            else :
                                BrickPi.MotorSpeed[port[1]] = 255
                            BrickPiUpdateValues()
                            if debug : 
                                print "Motor 1 speed : " + `BrickPi.MotorSpeed[port[1]]`
                                print "Speed Multiplier : " + `smulti`

                        else :
                            #Motor 0 CAN be sped up
                            smulti=BrickPi.MotorSpeed[port[0]]*delta*gain+idelta*gain
                            #Speed Multiplier: the amount we want to speed up Motor 0
                            if int(abs(BrickPi.MotorSpeed[port[0]]+smulti)) <= 255 :   
                                #Target speed is inside the bounds of Motor speed
                                BrickPi.MotorSpeed[port[0]] = int (BrickPi.MotorSpeed[port[0]]+smulti)
                            elif int (BrickPi.MotorSpeed[port[0]]+smulti) < 0 :
                                #Target speed is outside the bounds of -255 to 255
                                BrickPi.MotorSpeed[port[0]] = -255 
                            else :
                                BrickPi.MotorSpeed[port[0]] = 255
                            BrickPiUpdateValues()
                            if debug : 
                                print "Motor 0 speed : " + `BrickPi.MotorSpeed[port[0]]`
                                print "Speed Multiplier : " + `smulti`


                    elif (last_encod[0] - curr_val[0]) > abs(last_encod[1] - curr_val[1]) :
                        #Motor 0 is going faster
                        delta= abs(curr_val[0]-last_encod[0])- abs(curr_val[1]-last_encod[1]) 
                        idelta = (abs(curr_val[0]-init_val[0]) - abs(curr_val[1]-init_val[1]))/alpha
                        if debug :
                                print "Motor 0 is faster by "  + `delta`
                                print "last_encod = " + `last_encod[0]` + " , " + `last_encod[1]`
                                print "idelta = " + `idelta`
                                print "Current Encode = " + `curr_val[0]` + " , " + `curr_val[1]`

                        if abs(BrickPi.MotorSpeed[port[1]]) == 255 :
                            #Motor 1 CANNOT be sped up, SLOW DOWN Motor 0
                            smulti=BrickPi.MotorSpeed[port[0]]*delta*gain+idelta*gain
                            #Speed Multiplier: the amount we want to slow down Motor 0
                            if int(abs(BrickPi.MotorSpeed[port[0]]-smulti)) <= 255 :
                                #Target speed is inside the bounds of Motor
                                BrickPi.MotorSpeed[port[0]] = int (BrickPi.MotorSpeed[port[0]]-smulti)
                            elif int (BrickPi.MotorSpeed[port[0]]-smulti) < 0 :
                                #Target speed is outside the -255 to 255 bounds
                                BrickPi.MotorSpeed[port[0]] = -255
                            else : 
                                BrickPi.MotorSpeed[port[0]] = 255
                            BrickPiUpdateValues()
                            if debug : 
                                print "Motor 0 speed : " + `BrickPi.MotorSpeed[port[0]]`
                                print "Speed Multiplier : " + `smulti`

                        else :
                            #Motor 1 CAN be sped up SPEED UP Motor 1
                            smulti=BrickPi.MotorSpeed[port[0]]*delta*gain+idelta*gain
                            #Speed Multiplier: the amount we want to speed up Motor 1
                            if int(abs (BrickPi.MotorSpeed[port[1]]+smulti)) <= 255 :
                                #Target speed is inside the bounds of Motor
                                BrickPi.MotorSpeed[port[1]] = int (BrickPi.MotorSpeed[port[1]]+smulti)
                            elif int (BrickPi.MotorSpeed[port[1]]+smulti) < 0 :
                                #Target speed is outside the -255 to 255 bounds
                                BrickPi.MotorSpeed[port[1]] = -255
                            else :
                                BrickPi.MotorSpeed[port[1]] = 255
                            BrickPiUpdateValues()
                            if debug : 
                                print "Motor 1 speed : " + `BrickPi.MotorSpeed[port[1]]`
                                print "Speed Multiplier : " + `smulti`
                        
            last_encod[0] = curr_val[0]
            last_encod[1] = curr_val[1]
            
    for i in range(num_motor):
        print "Motor " + `i` + " encoder done: " + `init_val[i]`
    return 0

def main():
    time.sleep(1)
    power=[255,255]
    deg=[-68000,-68000]
    port=[PORT_B,PORT_C]
    print "Starting Forward"

    motorRotateDeg(power,deg,port)

    print "Forward Done"
    return 0

if __name__ == "__main__":
    main()