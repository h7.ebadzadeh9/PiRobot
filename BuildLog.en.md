# BuildLog
Read this in different language: [English](README.md),[Polish](README.pl.md)

###### 17.10.2016
I bought gyro sensor and consider using Raspberry Pi + BrickPi.

###### 25.10.2016 1:28
NXT does not work. New batteries did not help. I can hear very quiet buzzing inside.
I joined RPI, prickpi and motor. Whole day fighting java for nothing. Motor does not work at all.
With Python it worked right away. Maybe leave java and write it in python?

###### 25.10.2016 17:36
The fight against RPI continues. I have two images

1. Some raspbiana flavour provided by DexterInd, creators of the BrickPi - python is preinstalled and configured, Eclipse is too slow, but jars might work. And motors works. That's nice.
2. Ubuntu Mate - I installed java, eclipse, python - When checking if it works with python I got this:
`IOError: [Errno 25] Inappropriate ioctl for device`

###### 27.10.2016 20:10
http://forum.dexterindustries.com/t/rpi3-ubuntu-serial-connection-error/1982/4

###### 28.10.2016 10:22
As you can see in the above link, developers do not support ubuntu. Therefore, I will use their raspbpian.

###### 28.10.2016 23:33
NXT is working, however, screen is not working. I have installed lejos but I can't do anything else because I don't know menu and don't want to use it blindly. If it will not work with BrickPi to try to resolder the screen, see if that will fix it ...
With BrickPi it goes hard. I do not quite know what library I should use to just rotate simple motor. On raspbian I tried to fire up the program from the brickpijava repo but it did not work. I also tried to run ev3dev on RPI but it did not start. I don't know why. More and more inclined to raspbpian + python, only here I managed to run anything.

###### 06.11.2016 14:12
BrickPi needs an update to handle gyroscope. I need to find someone with arduino uno.
I have to give up Java. I can not run it on the RPI. I do not know why but somehow these libraries can not detect and handle the equipment. Python works without a problem. Just to fix the gyro...

###### 14.11.2016 22:42
Today I tried to update brickpi firmware. It did not work out too well.
`avrdude: stk500_getsync () not in sync: resp = 0x00`
Will try againg on Friday. I think I know what I did wrong - without the power from RPI, brickpi must be powered from batteries. I do not know if that is the cause of the failure though.

###### 16.11.2016 21:05
Why do I have so many problems?
I borrowed programmer USBasp;
Plugged it in PC - windows can not see it;
Reinstalled all drivers, ran the script update - avrdude not see the device;
Rummaged in the net, changed device in the script to USBasp and ran - still nothing;
Rummaged in the net, someone said to install another driver, so I did - windows screams about dangerous driver;
Reboot, turned off checking drivers signatures, installation successful, ran the script - `cannot set SCK period`....
On the net again, they said to ignore. But what I'm getting as a result from avrdude does not match what dexter ind had.
Turning the engine still works, gyro still does not.
[Link to the forum where I am looking for help] (http://forum.dexterindustries.com/t/updating-firmware-to-the-latest-version/1763/12)

###### 22.11.2016 17:07
Still looking for help on the forum.

###### 28.11.2016 10:42
http://forum.dexterindustries.com/t/update-brickpi-fw-with-usbasp/2122/4
On Friday I created a separate topic on the forum. Today I got a reply. It turns out the script that I used did not run updating commands for some reason.
It just did something to fuses. Whatever that means... I did it manually and the results were promising. However, python test returned error.
The effect is such that now even moving the motor does not work. I'll wait for see creators' answer.

###### 5.12.2016 11:45
The creators wrote back and showed me where I made a mistake. I missed EEPROM in one line. However, improved line did not help too much.
Now the program is stuck on checking sensors. I will try to return to the latest working configuration and wait for their response.
http://forum.dexterindustries.com/t/update-brickpi-fw-with-usbasp/2122/5

###### 7.12.2016 18:26
Nothing works. Arduino will not even budge loading an update. USBasp does not do, and I can not run anything on rpi.
Slowly, I've had enough. I have nothing done beside the robot and the time is running out.

###### 10.12.2016 11:27
Yesterday, I borrowed another USBasp from another friend. Update ran much longer, about 5 min, but it did not work out. Now programs hang on BrickPiSetupSensors().
I tried arduino again. Previously, I didn't upload ISP program on it so nothing worked. This time I did the steps 1-4 of [this tutorial](https://www.arduino.cc/en/Tutorial/ArduinoISP), and then I have uploaded fw on brickpi. The effect is the same as the previous few attempts - hangs on BrickPiSetupSensors()

###### 10.12.2016 19:08
Somewhere along the way, I must've done `apt-get upgrade` because I ended up with PIXEL raspbian.
It turns out the PIXEL somehow affects and interferes with GPIO so BrickPi creators suggested to return to their latest image.
The good news - motors rotate again
Bad message - `DOH! Please update your firmware to run EV3 sensors! You should be able to run NXT sensors, but not EV3 sensors.`
So gyroscope does not work

###### 13.12.2016 12:13
[IT WORKS!!!!!!! FINALLY WORKS](http://forum.dexterindustries.com/t/update-brickpi-fw-with-usbasp/2122/23?u=mike2060)

###### 13.12.2016 13:17
Power Problems. Strange things happened to my rechargeable batteries. One changed polarizations, 3 are dead and 4 have more than 1.2V. Loading batteries ...

###### 13.12.2016 15:38:30
Are these Chinese rechargeable batteries may be regular batteries? Eacho one has a 1.5V ...
Motors still work :)

###### 13.12.2016 18:36:44
motorRotateDegree turns forever...

###### 13.12.2016 19:07:25
[Kill me ...](http://forum.dexterindustries.com/t/accuracy-of-rotating-a-motor/258/20)

###### 14.12.2016 8:01:02
I was creating the environment on a laptop, setting pycharm, adding a remote server (RPI) and setting the remote interpreter.
Interpreter begun update. After 10 minutes I gave up because I had no time for this. And the robot had stopped working.
Motors did not work, even in SetupSenors() didn't. I reinstalled BrickPi(library).

###### 14.12.2016 8:40:00
I went to consultions with non-operating robot. By January still will compete. Maybe I could do different, less complicated robot.

###### 14.12.2016 9:20:00
Before next class I sat down and started to check that did not work. I wrote the same commands and it worked! ... now no one will know why it have not worked before.

###### 17.12.2016 18:53:09
It's not good...

Method updateValues ​​() at the same time, turns the servo and reads from the sensors (gyroscope). It is unacceptable for the self-balancing robot.

A simple project like simplebot is not working correctly. Below are the commands, what they should do, and what is actually happening.
The code is available here: [github link] (https://github.com/DexterInd/BrickPi_Python/blob/master/Project_Examples/simplebot/simplebot_simple.py)

| command | What should happen | What's happening |
|:-------:|:--------------------:|:-------------:|
|w        |both motors forward   | right forward |
|a        |right motor forward   | left forward  |
|d        |left motor front      | nothing       |
|s        |both motors back      | nothing       |
|x        |quit                  | both backward |
|ctrl+c   |quit                  | quits but the motors continue to rotate |
|a as the first command after start|right motor forward  |both motors back|
[Robot controls - simplebot_simple]

Previous command affects the behavior of next. `s` after `w` will stop motors.

I started another thread on their forums available [here] (http://forum.dexterindustries.com/t/serious-problems-with-brickpi/2211)
I suspect it will take some time. Meanwhile, I'll try to upload different library - ev3dev.

###### 19.12.2016 15:45:53
I tried the proposed code and it is not better. First, both motors rotate forward a little, and then only one turns backwards forever.

###### 19.12.2016 23:44:12
Graykevinb from forum reached out to me and sent me his code for brickpi (graykevinb_solution.py). His solution of motorRotateDegree did not work for me as well. Maybe the problem is hardware? My motors might be too old? Maybe I should use newer EV3 ones?

###### 23.12.2016 23:32:20
EV3DEV. I have prepared sd card and ran it (not without problems) - the system works.
What about the rest? Nothing really works :D Right after rpi3 start and after connecting the battery motors begin to spin. On their own.
I searched for some python processes but none is running. I don't know what to do with this.
Example applet to rotate motor throws "device is not connected"
[Link to git issues] (https://github.com/rhempel/ev3dev-lang-python/issues/279)

###### 27.12.2016 18:19:34
I was told to create new git issue. So far nothing works.
[New link] (https://github.com/ev3dev/ev3dev/issues/796)

###### 27.12.2016 23:19:42
It is better. Motors are working. That means I am able to make simplebot. Sensors still not working. It can not detect them somehow.
`Exception: Device is not connected`
[New link] (https://github.com/ev3dev/ev3dev/issues/797)

###### 28.12.2016 12:56:55
BrickPi on ev3dev does not autodetect sensors. Motors are loaded on boot but sensors need to be loaded manually. That's why it didn't work.
I was able to connect one touch sensor. That's something. I have a problem connecting second one. I still do not know what the gyro.
[Link](https://github.com/ev3dev/ev3dev/issues/797)

###### 28.12.2016 21:37:42
Making good progress, I know how to load two sensors via python. Gyro does not collect the correct data. But I can create simple, driving robot.
[Link] (https://github.com/rhempel/ev3dev-lang-python/issues/281)

###### 29.12.2016 20:09:37
HAH! Finally works!
This is not a balancing robot but the robot controlled by the browser. Still counts right?
[Video](https://1drv.ms/v/s!AtR5ruC8xD0VgudhDcSQ3u0ntGLK8Q)

###### 29.12.2016 22:46:50
Cleaning up the files. I decided to create english version of my BuildLog so that more people could learn from my mistakes. Eh.. now I have to add each new entry twice :(

###### 30.12.2016 11:35:04
DexterInd made an update few days ago and I decided to check it out. Unfortunately motorRotateDegree is still not working properly (or I messed something up).
Motor power influences angle, which in its own is not perfect.
At pwr=100, deg=90 actual rotation is almost 240deg.
At pwr=200,deg=90 it is over 450deg.
ev3dev programs cannot be run on DexterInd's image.

###### 30.12.2016 17:10:08
I finally managed to add 'emergencyBreak' - when robot hits a wall it stops, and 'closeBtn' to manually stop and **exit** program.